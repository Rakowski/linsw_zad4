import os
import tornado.ioloop
import tornado.web
import base64
from subprocess import call

FILES = '/mnt/files'
FIFO = '/root/mplayer_fifo'
MM = None

class MplayerManager:
    def send(self, msg):
        os.system('echo "' + msg + '" > ' + FIFO)

    def setVolume(self, val):
       send('volume ' + val + '1')

    def toggleMute(self):
        send('mute')

    def pause(self):
        send('pause')

    def stop(self):
        send('stop')

class Index(tornado.web.RequestHandler):
    def get(self):
	files = []
	for filename in os.listdir(FILES):
		files.append(filename)
	self.render('index.html', files=files)

    def post(self):
        auth_hdr = self.request.headers.get('Authorization')
        print auth_hdr
        if auth_hdr == None or not auth_hdr.startswith('Basic '):
            return self.auth()

        auth_decoded = base64.decodestring(auth_hdr[6:])
        username, password = auth_decoded.split(':', 2)

        if username != "ja" or password != "ja":
            return self.auth()

        f = self.request.files['datafile'][0]
        filename = f['filename']
    	
        print "Upload request with file: " + filename
        x = open(FILES + filename, 'w')
    	x.write(f['body'])
    	x.close()

    	self.redirect("/")

    def auth(self):
        self.set_status(401)
        self.set_header('WWW-Authenticate', 'Basic realm=Fileserver')
        self.finish()
        return False

class VolumeChanged(tornado.web.RequestHandler):
    def post(self, vol):
        MM.setVolume(vol)

class MuteToggled(tornado.web.RequestHandler):
    def post(self):
        MM.toggleMute()

class Paused(tornado.web.RequestHandler):
    def post(self):
        MM.pause()

if __name__ == "__main__":
    MM = MplayerManager()

    webserv = tornado.web.Application([
                (r"/", Index),
                (r"/volume_changed/(1?[0-9]?[0-9])", VolumeChanged),
                (r"/mute/", MuteToggled),
                (r"/pause/", Paused)
        ])

    webserv.listen(80)
    tornado.ioloop.IOLoop.instance().start()
